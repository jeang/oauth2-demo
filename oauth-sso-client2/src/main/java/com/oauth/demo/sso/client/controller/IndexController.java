package com.oauth.demo.sso.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lijinyang on 2020/3/6.
 */
@Slf4j
@Controller
public class IndexController {

    @RequestMapping("loginUserInfo")
    @ResponseBody
    public Object loginUserInfo(Principal principal, HttpServletRequest request) {
        if (null == principal) {
            log.info("====> 用户未登录");
            return null;
        }
        HttpSession session = request.getSession(false);
        log.info("====> session id is {}", session.getId());
        return principal;
    }

    /*@RequestMapping("logout")
    @ResponseBody
    public Object logOut(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (null != session) {
            session.invalidate();
        }
        Map<String, String> resMap = new HashMap<>();
        resMap.put("code", "200");
        return resMap;
    }*/
}
