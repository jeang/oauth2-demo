package com.oauth.demo.sso.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by lijinyang on 2020/3/6.
 */
@SpringBootApplication
public class OauthDemoSsoClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthDemoSsoClientApplication.class, args);
    }
}
