package com.oauth.client.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2019/9/16.
 */
@Slf4j
@Controller
public class HomeController {

    /**
     * 用于sso登录，才会获取到userPrincipal信息
     * @param request
     * @return
     * @throws Exception
     */
    @GetMapping("/")
    @ResponseBody
    public Object getUserInfo(HttpServletRequest request) throws Exception {
        Principal userPrincipal = request.getUserPrincipal();
        String name = userPrincipal.getName();
        Map<String, String> resmap = new HashMap<>();
        resmap.put("userName", name);
        return resmap;
    }

    @GetMapping("/test")
    @ResponseBody
    public Object getUserInfoTest(Principal principal) throws Exception {
        log.info("====> {}", principal.toString());
        String name = principal.getName();
        Map<String, String> resmap = new HashMap<>();
        resmap.put("userName", name);
        return resmap;
    }

    /**
     * 转发接口用于redirect跳转
     * @param originPage
     * @throws Exception
     */
    @GetMapping("/tologin")
    public String toLogin(String originPage) throws Exception {
        return "redirect:" + originPage;
    }
}
