package com.oauth2.demo.tests.service;

import com.alibaba.fastjson.JSON;
import com.oauth2.demo.config.service.OauthClientDetailsServiceImpl;
import com.oauth2.demo.tests.EnvTestsApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetails;

/**
 * Created by Administrator on 2019/9/7.
 */
@Slf4j
public class OauthServiceTests extends EnvTestsApplication{

    @Autowired
    private OauthClientDetailsServiceImpl oauthClientDetailsService;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    @Test
    public void testGetClient() throws Exception {
        ClientDetails clientDetails = oauthClientDetailsService.loadClientByClientId("7gBZcbsC7kLIWCdELIl8nxcs");
        log.info("=====> {}", JSON.toJSONString(clientDetails));
    }

    @Test
    public void testEncode() throws Exception {
        log.info("====> {}", passwordEncoder.encode("E2DA5B2DEDD548AEB7ABA689436C2E2C"));
    }
}
