package com.oauth2.demo.tests.dao;

import com.alibaba.fastjson.JSON;
import com.oauth2.demo.dao.OauthClientDetailsDao;
import com.oauth2.demo.mapper.entity.OauthClientDetails;
import com.oauth2.demo.tests.EnvTestsApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.List;

/**
 * Created by Administrator on 2019/9/7.
 */
@Slf4j
public class OauthDaoTests extends EnvTestsApplication{

    private OauthClientDetailsDao detailsDao;

    /**
     * 测试类，获取OauthClient
     * @throws Exception
     */
    @Test
    public void testGetOauthClient() throws Exception {
        OauthClientDetails oauthClientDetailss = detailsDao.selectByPrimaryKey("11123");
        log.info("====> {}", JSON.toJSONString(oauthClientDetailss));

    }

}
