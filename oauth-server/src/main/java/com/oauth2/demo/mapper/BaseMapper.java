package com.oauth2.demo.mapper;

import tk.mybatis.mapper.additional.insert.InsertListMapper;
import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;

/**
 * Created by Administrator on 2019/9/6.
 * BaseMapper不能引入到MapperScann中，单独引出来，泛型是不能Bean化的
 */

public interface BaseMapper<T> extends Mapper<T>, IdsMapper<T>, InsertListMapper<T>{
}
